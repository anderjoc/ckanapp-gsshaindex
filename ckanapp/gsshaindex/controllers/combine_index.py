from ckan.lib.base import BaseController
from ckan.lib import helpers as h
import ckan.plugins as p
from ..models import Jobs, jobs_sessionmaker, gsshapy_engine, gsshapy_sessionmaker
import json
import ckanapp.gsshaindex.lib as gi_lib
import os
import ConfigParser
from gsshapy.orm import ProjectFile, IndexMap, RasterMapFile, MTIndex, MTValue
from pylons.controllers.util import redirect
from sqlalchemy import distinct
import time
from mapkit.RasterConverter import RasterConverter
import pprint


# Get app.ini
controller_dir = os.path.abspath(os.path.dirname(__file__))
config_path = os.path.join(os.path.abspath(os.path.dirname(controller_dir)), 'app.ini')
app_config = ConfigParser.RawConfigParser()
app_config.read(config_path)

maps_api_key = app_config.get('api_key', 'maps_api_key')

class CombineController(BaseController):
    def combine_index (self):
            # Tools
            t = p.toolkit
            c = t.c
            _ = t._
            
            job_id = c.job_id
            
            # Find user id
            user_id = c.author or c.user
            password = c.password
            
            c.map_name = c.map_name
            
            # Get the job from the database
            job_session = jobs_sessionmaker()
            job = job_session.query(Jobs).\
                            filter(Jobs.user_id == user_id).\
                            filter(Jobs.id == job_id).one()
                            
            #Create job_session
            gsshapy_session = gsshapy_sessionmaker()
            
            # Get project file id                
            project_file_id = job.new_model['project_file_id']
            
            # Get list of index files
            c.resource_kmls = json.loads(job.current_kmls)
            
            c.resource_name = []
            c.resource_url = []
            c.resource_info = []
            
            # Get array of names and urls
            for key in c.resource_kmls:
                if key != c.map_name:
                    c.resource_name.append(key)
                    c.resource_url.append(c.resource_kmls[key]['url'])
#                     c.resource_info.append((key, c.resource_kmls[key]['url']))
                    c.resource_info.append((key,key))
   
            c.select_input1 = {'display_text': "Select first index map",
                               'name': 'select1',
                               'multiple': False,
                               'options': c.resource_info}
            
            c.select_input2 = {'display_text': "Select second index map or none",
                               'name': 'select2',
                               'multiple': False,
                               'options': [("None", "none")] + c.resource_info}
                
            params = t.request.params
            
            project_file = gsshapy_session.query(ProjectFile).filter(ProjectFile.id == project_file_id).one()
            index_raster = gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == c.map_name).one()
            mapTables = index_raster.mapTables
            
            # Process if the submit button is pressed
            if 'submit_combo' in params:
                # Error message if both maps selected are the same
                if params['select1']==params['select2']:
                    h.flash_error("You must select two different index maps. Or if you'd like to replace this map with a different index map, select None for the second map")
                # Process if only one map is selected
                elif params['select2']=="none":
                    select1_id = gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == params['select1']).one()
                    statement = '''UPDATE idx_index_maps
                                      Set raster = ST_MapAlgebra(
                                      (SELECT raster FROM idx_index_maps WHERE id = '''+ unicode(select1_id.id) +'''), 1,
                                      (SELECT raster FROM idx_index_maps WHERE id = '''+ unicode(index_raster.id) +'''), 1,
                                      '([rast1]*1000 + [rast2]*0)'
                                      )
                                    WHERE id = '''+ unicode(index_raster.id) +''';
                                '''
                    result = gsshapy_engine.execute(statement)
                # Process if two maps are selected
                else:
                    # Get the ids for the two index maps to be combined
                    select1_id = gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == params['select1']).one()
                    select2_id = gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == params['select2']).one()
                    # Combine the maps and give a unique id
                    statement = '''UPDATE idx_index_maps
                                      SET raster =ST_MapAlgebra(
                                      (SELECT raster FROM idx_index_maps WHERE id = '''+ unicode(select1_id.id) +'''), 1,
                                      (SELECT raster FROM idx_index_maps WHERE id = '''+ unicode(select2_id.id) +'''), 1,
                                      '(([rast1]*1000) + [rast2])'
                                      )
                                    WHERE id = '''+ unicode(index_raster.id) +''';
                                '''
                    result = gsshapy_engine.execute(statement)
                
                # Get the values in the index map
                statement3 = '''SELECT (pvc).*
                                FROM (SELECT ST_ValueCount(raster,1,true) As pvc
                                FROM idx_index_maps WHERE id = '''+ unicode(index_raster.id) +''') AS foo
                                ORDER BY (pvc).value;
                                '''
                result3 = gsshapy_engine.execute(statement3)
                
                # Get the indices for the index being changed
                indices = gsshapy_session.query(distinct(MTIndex.index), MTIndex.id, MTIndex.description1, MTIndex.description2).\
                                       join(MTValue).\
                                       filter(MTValue.mapTable == mapTables[0]).\
                                       order_by(MTIndex.index).\
                                       all()
                
                # Go through the map tables that use the index map
                map_table_count = 0
                for mapping_table in mapTables:

                    # Reset the number of ids to start counting them            
                    numberIDs = 0
                    ids = []
                
                    # Go through each new id value
                    for row in result3:
                        index_present = False
                        numberIDs +=1
                        ids.append(row.value)
                        large_id = int(row[0])
                        for index in index_raster.indices:
                            if int(index.index) == int(row[0]):
                                index_present = True
                                break
                        
                        if index_present == False:
                            if str(large_id).endswith("000") == False:
                                second_id = str(large_id).split("0")[-1]
                                first_id = (large_id - int(second_id))/1000
                            else:
                                first_id = (large_id)/1000
                                second_id = ""
                                description2 = ""

                            pastinfo1 = gsshapy_session.query(distinct(MTIndex.index), MTIndex.id, MTIndex.description1, MTIndex.description2).\
                                    filter(MTIndex.idxMapID == select1_id.id).\
                                    filter(MTIndex.index == first_id).\
                                    all()
                            description1 = pastinfo1[0].description1 + " " + pastinfo1[0].description2  
                            
                            if second_id != "":
                                pastinfo2 = gsshapy_session.query(distinct(MTIndex.index), MTIndex.id, MTIndex.description1, MTIndex.description2).\
                                        filter(MTIndex.idxMapID == select2_id.id).\
                                        filter(MTIndex.index == second_id).\
                                        all()
                                description2 = pastinfo2[0].description1 + " " + pastinfo2[0].description2

                            # Create new index value  
                            new_indice = MTIndex(row[0], description1, description2)
                            new_indice.indexMap = index_raster
                            for mapping_table in mapTables:
                                distinct_vars = gsshapy_session.query(distinct(MTValue.variable)).\
                                                 filter(MTValue.mapTable == mapping_table).\
                                                 order_by(MTValue.variable).\
                                                 all()
                                                
                                variables = []
                                for var in distinct_vars:
                                    variables.append(var[0])
                                    
                                for variable in variables:
                                    new_value = MTValue(variable, 0)
                                    new_value.mapTable = mapping_table
                                    new_value.index = new_indice
    
                                gsshapy_session.commit()
                            
                    # Delete indices that aren't present
                    for index in indices:
                        if not int(index[0]) in ids:
                            bob = gsshapy_session.query(MTIndex).get(index.id)
                            for val in bob.values:
                                gsshapy_session.delete(val)
                            gsshapy_session.delete(bob)
                    gsshapy_session.commit()
                            
                    index_raster.mapTables[map_table_count].numIDs = numberIDs  
                    gsshapy_session.commit()      
                    map_table_count +=1
                
                indices = gsshapy_session.query(distinct(MTIndex.index), MTIndex.id, MTIndex.description1, MTIndex.description2).\
                                       join(MTValue).\
                                       filter(MTValue.mapTable == mapTables[0]).\
                                       order_by(MTIndex.index).\
                                       all()

                    
                index_raster =  gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == c.map_name).one()
                
                # Specify workspace
                controllerDir = os.path.abspath(os.path.dirname(__file__))
                gsshaindexDir = os.path.abspath(os.path.dirname(controllerDir))
                publicDir = os.path.join(gsshaindexDir, 'public')
                publicUser = os.path.join(publicDir, str(user_id))
                indexMapDir = os.path.join(publicUser, 'index_maps')
                
                # Create kml file name and path
                current_time = time.strftime("%Y%m%dT%H%M%S")
                resource_name = index_raster.name + "_" + str(user_id) + "_" + current_time
                kml_ext = resource_name + '.kml'
                clusterFile = os.path.join(indexMapDir, kml_ext)
                
                # Generate color ramp
                index_raster.getAsKmlClusters(session=gsshapy_session,
                                               path = clusterFile,
                                               colorRamp = RasterConverter.COLOR_RAMP_HUE,
                                               alpha=1.0)
                
                resource, status = gi_lib.add_kml_CKAN(user_id, password, c, clusterFile, resource_name, index_raster.name)
                
                temp_list = json.loads(job.current_kmls)  
                
                if status == True:
                    for item in temp_list:
                            if item == c.map_name:
                                del temp_list[item]
                                temp_list[c.map_name] = {'url':resource['url'], 'full_name':resource['name']}
                                break
        
                job.current_kmls = json.dumps(temp_list)
                job_session.commit()
                
                redirect(t.url_for('gsshaindex-mapping-table', job_id=c.job_id, map_name=c.map_name, mapping_table="0"))
                    
            
            # Set the first index as the active one
            c.map_options = str(c.resource_name[0])
            
            # Set up map properties
            c.editable_map = {'height': '400px',
                              'width': '100%',
                              'reference_kml_action': '/apps/gssha-index/get-index-maps/' + c.job_id + '/' + c.map_options,
                              'maps_api_key':maps_api_key,
                              'drawing_types_enabled':[]}
                            
            return t.render('gsshaindex/combine_index.html')