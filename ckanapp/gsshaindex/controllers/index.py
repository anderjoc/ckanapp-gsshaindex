from ckan.lib.base import BaseController, abort
from ckan.lib import helpers as h
import ckan.plugins as p
from ckanext.tethys_apps.lib.app_harvester import SingletonAppHarvester
from ckanext.tethys_apps.lib import get_app_definition, set_session_global, set_session_globals, get_session_global
from ckanext.tethys_apps.lib.dataset_tools import *
from ckanext.tethys_apps.lib import ckanclient_ppp as ckanclient
from pylons.decorators import jsonify
from pylons.controllers.util import redirect
import pylons.config as config
import ckan.plugins.toolkit as toolkit
import json
import os
import ConfigParser
import time
import zipfile
from gsshapy.orm import ProjectFile, IndexMap, RasterMapFile, MTIndex, MTValue, MapTableFile, MapTable
from gsshapy.lib import db_tools as dbt
from mapkit.RasterConverter import RasterConverter
import ckanapp.gsshaindex.lib as gi_lib 
import ckanapp.gsshaindex.lib.geoserver as geoserver 
from sqlalchemy import MetaData, create_engine, distinct
from sqlalchemy.orm import sessionmaker, relationship
import pprint
import pickle
import random
import operator

from ..models import Jobs, jobs_sessionmaker, gsshapy_sessionmaker

# Get app.ini information
controller_dir = os.path.abspath(os.path.dirname(__file__))
config_path = os.path.join(os.path.abspath(os.path.dirname(controller_dir)), 'app.ini')
app_config = ConfigParser.RawConfigParser()
app_config.read(config_path)
raster2pgsql_path = app_config.get('postgis', 'raster2pgsql_path')
maps_api_key = app_config.get('api_key', 'maps_api_key')

class GSSHAIndexController(BaseController):
    
    def index(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._

        context = {'user': c.user or c.author}
        user_id = c.user or c.author
         
        # Check permissions
        try:
            t.check_access('apps_read', context)
        except t.NotAuthorized:
            abort(401, _('Not authorized to see this page'))
            
        # Get App Definition
        app_definition = get_app_definition('gsshaindex')
        
        c.app_name = app_definition['name']
        c.app_index = app_definition['index']
        

        
        # Check if the submit button has been clicked and if there is a file_id
        if ('submit_gssha' in t.request.params):
            params = t.request.params
            print params
            c.file_id = params['file_id']
            file_name = params['file_name']
            file_url = params['file_url']
            print file_url
            if (c.file_id == ""):
                h.flash_error('No GSSHA Model is selected')
            else:
                session = jobs_sessionmaker()
                new_job = Jobs(name = file_name, user_id = context['user'],original_model = {'id':c.file_id, 'url':file_url, 'name':file_name})
                session.add(new_job)
                session.commit()
                c.job_id = new_job.id
                redirect(t.url_for('gsshaindex-extract-gssha', job_id=c.job_id))
        
        # Create empty dictionary of cached masks if it doesn't exist
        try: 
            get_session_global('mask_cache')
        except:
            mask_cache = {}
            set_session_global('mask_cache', mask_cache)
             
        # Find all the GSSHA models in the datasets
        results = get_resource_by_field_value('model:GSSHA')
        
        # Create empty array to hold the information for the GSSHA models
        c.model_list = []
        
        no_files = True
        
        # Fill the array with information on the GSSHA models
        if results.get('count') > 0:
            no_files = False
            for result in results.get('results'):
                if result.get('description')=="":
                    description = "None"
                else:
                    description = result.get('description')
                c.model_list.append({"name":result.get('name'), "id":result.get('id'), "description":description, "url":result.get('url')})
        
        if no_files == False:
                
            c.model_list.sort(key=operator.itemgetter('name'))  
            c.file_id = c.model_list[0]['id']
            c.file_name = c.model_list[0]['name']
            c.file_url = c.model_list[0]['url']
                    
            #Display a google map with kmls
            c.editable_map = {'height': '500px',
                            'width': '100%',
                            'reference_kml_action': '/apps/gssha-index/get-mask-map/' + str(c.file_id),
                            'maps_api_key':maps_api_key,
                            'drawing_types_enabled':[]}
            
        else:
            #Display a google map with kmls
            c.editable_map = {'height': '500px',
                            'width': '100%',
                            'maps_api_key':maps_api_key,
                            'drawing_types_enabled':[]}
        
        # Display the index page        
        return t.render('gsshaindex/index.html')
    
    @jsonify
    def get_mask_map(self):
        '''
        Retrieve the mask map for model preview
        '''  
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._ 
        
        # Find user id
        user_id = c.author or c.user
        password = c.password
        
        # Specify workspace
        controllerDir = os.path.abspath(os.path.dirname(__file__))
        gsshaindexDir = os.path.abspath(os.path.dirname(controllerDir))
        workspace = os.path.join(gsshaindexDir,'workspace')
        userDir = os.path.join(workspace, str(user_id))
        indexMapDir = os.path.join(userDir, 'index_maps')
        
        # Clear the workspace
        gi_lib.clear_folder(userDir)
        
        # Get the mask_cache    
        mask_cache = get_session_global('mask_cache')
        
        # Display the mask if it's in the cache
        kml_link = gi_lib.find_in_cache(c.file_id, mask_cache)
        if kml_link != False:
#             return {'kml_link':['http://ciwmap.chpc.utah.edu:8080/geoserver/usa/wms?service=WMS&version=1.1.0&request=GetMap&layers=usa:states&styles=&bbox=-124.731,24.956,-66.97,49.372&width=780&height=330&srs=EPSG:4326&format=application/vnd.google-earth.kml+xml']}
            return {'kml_link':[kml_link]}
        
        # If the mask isn't in the cache, download it, turn it into a kml, add it to the cache, and display it
        else:
            # Get url for the resource
            results = get_resource_by_field_value('model:GSSHA')
            for result in results.get('results'):
                if result.get('id')== c.file_id:
                    url = result.get('url')
                    name = result.get('name')
                    norm_name = name.replace(" ","")
                    
                    # Extract location
                    extract_path = path.join(userDir, c.file_id)
                    
                    # Extract mask file
                    mask_file = gi_lib.extract_mask(url, extract_path)
                    
                    # If there isn't a mask file, return a blank list of kmls
                    if mask_file == "blank":
                        return {'kml_link': []}
                        
                    else:
                        # Remove the public kml file if it exists and replace it
                        gi_lib.clear_folder(indexMapDir)
                        
                        #Create session
                        gsshapy_session = gsshapy_sessionmaker()
                        
                        # Set up kml file name and save location
                        current_time = time.strftime("%Y%m%dT%H%M%S")                    
                        kml_name = norm_name + "_" + user_id + "_" + current_time
                        kml_ext = kml_name + ".kml"
                        kml_file = os.path.join(indexMapDir, kml_ext)
                        
                        colors = [(237,9,222),(92,245,61),(61,184,245),(171,61,245),(250,245,105),(245,151,44),(240,37,14),(88,5,232),(5,232,190),(11,26,227)]
                        
                        color = [random.choice(colors)]
                        
                        # Extract mask map and create kml
                        mask_map = RasterMapFile(extract_path, mask_file, gsshapy_session)
                        mask_map.read(spatial=True, spatialReferenceID=26912, raster2pgsqlPath = raster2pgsql_path)
                        mask_map.getAsKmlClusters(session=gsshapy_session, path=kml_file, colorRamp={'colors':color, 'interpolatedPoints':1})
                        
                        # Add mask kml to CKAN for viewing
                        resource, success = gi_lib.add_kml_CKAN(user_id, password, c, kml_file, kml_name, kml_name)
                        
                        # Check to ensure the resource was added
                        if success == True:
                            kml_link = resource['url']
                            # Add to the mask_cache
                            mask_cache[c.file_id] = kml_link
                            set_session_global('mask_cache',mask_cache)
                            return {'kml_link': [kml_link]}
    
    def extract_gssha(self):
        '''
        This takes the file name and id that were submitted and unzips the fies, 
        finds the index maps, and creates kmls.
        '''
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._

        # Get the fileId
        job_id = c.job_id

        # Find user id
        user_id = c.author or c.user
        password = c.password
        
        # Get the job and project file id from the database
        session = jobs_sessionmaker()
        job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()

        # Specify workspace
        controllerDir = os.path.abspath(os.path.dirname(__file__))
        gsshaindexDir = os.path.abspath(os.path.dirname(controllerDir))
        workspace = os.path.join(gsshaindexDir,'workspace')
        userDir = os.path.join(workspace, str(user_id))
        publicDir = os.path.join(gsshaindexDir, 'public')
        publicUser = os.path.join(publicDir, str(user_id))
        indexMapDir = os.path.join(publicUser, 'index_maps')
        
        # Clear workspace folder
        gi_lib.clear_folder(publicUser)
        gi_lib.clear_folder(userDir)
        gi_lib.clear_folder(indexMapDir)
              
        # Get url for the resource
        results = get_resource_by_field_value('model:GSSHA')
        for result in results.get('results'):
            if result.get('id')== job.original_model['id']:
                url = result.get('url')
        
        # Extract the GSSHA file
        extract_path, unique_dir = extract_zip_from_url(user_id, url, userDir)
        
                
        #Create session
        gsshapy_session = gsshapy_sessionmaker()
        
        # Find the project file
        for root, dirs, files in os.walk(userDir):
            for file in files:
                if file.endswith(".prj"):
                    project_name = file
                    project_path = os.path.join(root, file)
                    read_dir = os.path.dirname(project_path)
        
        # Create an empty Project File Object
        project = ProjectFile(directory=read_dir, 
                              filename=project_name, 
                              session=gsshapy_session)
        
        # Invoke the read command on the Project File Object
        project.readInput(spatial=True, spatialReferenceID=26912, raster2pgsqlPath = raster2pgsql_path)
        
        # Get dictionary to hold the kmls from this session
        current_kmls = {}
        
        # Store model information
        updated_new_model = {'project_file_id':project.id, 'project_name':project_name}
        job.new_model = updated_new_model
        
        # Get index maps
        index_list = gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project.mapTableFile).all()
        
        # Loop through the index
        for current_index in index_list:
            
            print current_index.name
            
            # Create kml file name and path
            current_time = time.strftime("%Y%m%dT%H%M%S")
            resource_name = current_index.name + "_" + str(user_id) + "_" + current_time
            kml_ext = resource_name + '.kml'
            clusterFile = os.path.join(indexMapDir, kml_ext)
            
            # Generate color ramp
            current_index.getAsKmlClusters(session=gsshapy_session,
                                           path = clusterFile,
                                           colorRamp = RasterConverter.COLOR_RAMP_HUE,
                                           alpha=1.0)
            
            resource, status = gi_lib.add_kml_CKAN(user_id, password, c, clusterFile, resource_name, current_index.name)
            
            # if the kml is added correctly, create an entry for the current_kmls with the name as the index name
            if status == True:
                current_kmls[current_index.name] = {'url':resource['url'], 'full_name':resource['name']}
       
        # Add the kmls with their url to the database
        job.current_kmls = json.dumps(current_kmls)
        session.commit()
        
        redirect(t.url_for('gsshaindex-select-index', job_id=c.job_id))
        
    
    def select_index(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        job_id = c.job_id
        
        # Find user id
        user_id = c.author or c.user
        
        # Get the job from the database
        session = jobs_sessionmaker()
        job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
        
        # Get project file id                
        project_file_id = job.new_model['project_file_id']
        
        # Get list of shapefiles
        shapefile_list = get_resource_by_field_value('model:Shapefile')
        c.shapefile_id = shapefile_list.get('results')[0]['id']
        print c.shapefile_id
        
        # Give options for editing the index map
        if ('select_index' in t.request.params):
            params = t.request.params
            c.map_name = params['index_name']
            if (params['method'] == "Create polygons"):
                redirect(t.url_for('gsshaindex-edit-index', map_name=c.map_name, job_id=c.job_id))
            elif (params['method'] == "Download shapefile"):
#                 h.flash_error("Select by polygon is currently in production and hasn't been initialized yet.")
                redirect(t.url_for('gsshaindex-shapefile-index', map_name=c.map_name, job_id=c.job_id, shapefile_id = c.shapefile_id))
            elif (params['method'] == "Merge index maps"):
                redirect(t.url_for('gsshaindex-combine-index', map_name=c.map_name, job_id=c.job_id))
        
        # Get list of index files
        c.resource_kmls = json.loads(job.current_kmls)
        
        # Create arrays of the names and urls
        c.resource_name = []
        c.resource_url = []
        for key in c.resource_kmls:
            c.resource_name.append(key)
            c.resource_url.append(c.resource_kmls[key]['url'])
        
        # Set the first index as the active one
        c.map_name = str(c.resource_name[0])
        
        #Create session
        gsshapy_session = gsshapy_sessionmaker()
        
        # Use project id to link to map table file
        project_file = gsshapy_session.query(ProjectFile).filter(ProjectFile.id == project_file_id).one()
        index_raster = gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == c.map_name).one()
        c.indices = index_raster.indices
        
        # Get project name for default value of new name
        c.project_name = job.name
        
        # Set up map properties
        c.editable_map = {'height': '600px',
                          'width': '100%',
                          'reference_kml_action': '/apps/gssha-index/get-index-maps/' + c.job_id + '/' + c.map_name,
                          'maps_api_key':maps_api_key,
                          'drawing_types_enabled':[]}
        
        return t.render('gsshaindex/select_index.html')
    
    @jsonify
    def get_index_maps(self):
        '''
        This action is used to pass the kml data to the google map. It must return the key 'kml_link'.
        '''
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Get the job id and user id
        job_id = c.job_id
        user_id = c.author or c.user
        
        # Get the job from the database
        session = jobs_sessionmaker()
        job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
        
        # Get the list of index files to display
        resource_list = json.loads(job.current_kmls)
        
        # Get the kml url 
        kml_link = resource_list[c.map_name]['url']

        return{'kml_link': [kml_link]}
        
           
    def mapping_table(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Find job id and user id
        job_id = c.job_id
        user_id = c.author or c.user
        
        # Get the job from the database
        session = jobs_sessionmaker()
        job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
        
        # Get project file id                
        project_file_id = job.new_model['project_file_id']
        
        # Get active index
        c.map_table_number = int(c.mapping_table)
        
        #Create session
        gsshapy_session = gsshapy_sessionmaker()
        
        # Use project id to link to map table file
        project_file = gsshapy_session.query(ProjectFile).filter(ProjectFile.id == project_file_id).one()
        index_raster = gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == c.map_name).one()
        c.indices = index_raster.indices
        mapTables = index_raster.mapTables
        
        # Process for if descriptions are submitted
        if ('submit_descriptions' in t.request.params):
            params = t.request.params
            for key in params:
                if "indice" in key:
                    if "desc1" in key:
                        identity = int(key.replace("indice-desc1-",""))
                        mapDesc1 = gsshapy_session.query(MTIndex).get(identity)
                        mapDesc1.description1 = params[key]

                    else:
                        identity = key.replace("indice-desc2-","")
                        mapDesc2 = gsshapy_session.query(MTIndex).get(identity)
                        mapDesc2.description2 = params[key]

            gsshapy_session.commit()
        
        # Get list of index files
        c.resource_kmls = json.loads(job.current_kmls)
        
        # Create array of kml names and urls
        c.resource_name = []
        c.resource_url = []
        for key in c.resource_kmls:
            c.resource_name.append(key)
            c.resource_url.append(c.resource_kmls[key]['url'])
        
        # Find the associated map tables and add them to an array
        c.mapTables = []
        count = 0
        for table in mapTables:
            name =  str(table.name)
            clean = name.replace("_"," ")
            c.mapTables.append([clean, table.name, count])
            count +=1
        
        # Find the variables that are related to the active map table
        distinct_vars = gsshapy_session.query(distinct(MTValue.variable)).\
                                     filter(MTValue.mapTable == mapTables[c.map_table_number]).\
                                     order_by(MTValue.variable).\
                                     all()
                                     
        # Create an array of the variables in the active map table
        c.variables = []
        for var in distinct_vars:
            c.variables.append(var[0])

        # Cross tabulate manually to populate the mapping table information
        c.indices = gsshapy_session.query(distinct(MTIndex.index), MTIndex.id, MTIndex.description1, MTIndex.description2).\
                               join(MTValue).\
                               filter(MTValue.mapTable == mapTables[c.map_table_number]).\
                               order_by(MTIndex.index).\
                               all()
                               
        # Get all the values to populate the table
        var_values = []     
        for var in c.variables:                 
            values = gsshapy_session.query(MTValue.id, MTValue.value).\
                                  join(MTIndex).\
                                  filter(MTValue.mapTable == mapTables[c.map_table_number]).\
                                  filter(MTValue.variable == var).\
                                  order_by(MTIndex.index).\
                                  all()
            var_values.append(values)
        c.values = zip(*var_values)
        
        # Dictionary of properties for the map
        c.google_map = {'height': '400px',
                          'width': '100%',
                          'kml_service': '/apps/gssha-index/get-index-maps/' +c.job_id+'/'+ c.map_name,
                          'maps_api_key':maps_api_key}

        return t.render('gsshaindex/mapping_table.html')
    
    def replace_values(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Find job id and user id
        job_id = c.job_id
        user_id = c.author or c.user
                
        # Get the job from the database
        session = jobs_sessionmaker()
        job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
                        
        # Get active index
        c.map_table_number = c.mapping_table
        
        # Get project file id                
        project_file_id = job.new_model['project_file_id']
        
        #Create session
        gsshapy_session = gsshapy_sessionmaker()
        
        # Use project id to link to map table file
        project_file = gsshapy_session.query(ProjectFile).filter(ProjectFile.id == project_file_id).one()
        index_raster = gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == c.map_name).one()
        c.indices = index_raster.indices
        c.mapTables = index_raster.mapTables
        
        # Update values with the user's edits in the database
        params = t.request.params
        for key in params:
            if "var" in key:
                identity = int(key.replace("var-",""))
                mapTableValue = gsshapy_session.query(MTValue).get(identity)
                mapTableValue.value = params[key]
        gsshapy_session.commit()
        
        redirect(t.url_for('gsshaindex-mapping-table', job_id=c.job_id, map_name=c.map_name, mapping_table=c.map_table_number))
    
    def submit_mapping_table(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Find job id and user id
        job_id = c.job_id
        user_id = c.author or c.user
        
        # Get active index
        c.map_table_num = int(c.mapping_table)
        
        # Get the job from the database
        session = jobs_sessionmaker()
        job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
        
        # Get project file id                
        project_file_id = job.new_model['project_file_id']
        
        # Get list of index files
        c.resource_kmls = json.loads(job.current_kmls)
        
        #Create session
        gsshapy_session = gsshapy_sessionmaker()
        
        # Use project id to link to map table file
        project_file = gsshapy_session.query(ProjectFile).filter(ProjectFile.id == project_file_id).one()
        index_raster = gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == c.map_name).one()
        c.indices = index_raster.indices
        mapTables = index_raster.mapTables
        
        # Find the associated map tables and add them to an array
        c.mapTables = []
        count = 0
        for table in mapTables:
            name =  str(table.name)
            clean = name.replace("_"," ")
            c.mapTables.append([clean, table.name, count])
            count +=1
        
        # Find the variables that are related to the active map table
        distinct_vars = gsshapy_session.query(distinct(MTValue.variable)).\
                                     filter(MTValue.mapTable == mapTables[c.map_table_num]).\
                                     order_by(MTValue.variable).\
                                     all()
                                     
        # Create an array of the variables in the active map table
        c.variables = []
        for var in distinct_vars:
            c.variables.append(var[0])
        
        # Cross tabulate manually to populate the mapping table information
        c.indices = gsshapy_session.query(distinct(MTIndex.index), MTIndex.description1, MTIndex.description2).\
                               join(MTValue).\
                               filter(MTValue.mapTable == mapTables[c.map_table_num]).\
                               order_by(MTIndex.index).\
                               all()
        
        # Get the values for the mapping table
        var_values = []
        for var in c.variables:                 
            values = gsshapy_session.query(MTValue.id, MTValue.value).\
                                  join(MTIndex).\
                                  filter(MTValue.mapTable == mapTables[c.map_table_num]).\
                                  filter(MTValue.variable == var).\
                                  order_by(MTIndex.index).\
                                  all()
            var_values.append(values)
        c.values = zip(*var_values)

        # Dictionary of properties for the map
        c.google_map = {'height': '400px',
                          'width': '100%',
                          'kml_service': '/apps/gssha-index/get-index-maps/' +c.job_id+'/'+ c.map_name,
                          'maps_api_key':maps_api_key}
        
        return t.render('gsshaindex/review_mapping_table.html')
    
    def zip_file(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Find job id and user id
        job_id = c.job_id
        user_id = c.author or c.user
        
        # Get the job from the database
        session = jobs_sessionmaker()
        job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
        
        # Get project file id                
        project_file_id = job.new_model['project_file_id']
       
        # Get the name and description from the submission
        params = t.request.params
        new_name = params['new_name']
        new_description = params['new_description']
        
        # Reformat the name by removing bad characters
        bad_char = "',.<>()[]{}=+-/\"|:;\\^?!~`@#$%&* "
        for char in bad_char:
            new_name = new_name.replace(char,"_")
        
        #Create session
        gsshapy_session = gsshapy_sessionmaker()
        
        # Get project from the database
        projectFileAll = gsshapy_session.query(ProjectFile).get(project_file_id)     
        
        # Create name for files
        project_name = projectFileAll.name
        if project_name.endswith('.prj'):
            project_name = project_name[:-4]
        pretty_date= time.strftime("%A %B %d, %Y %I:%M:%S %p")
        
        # Specify workspaces
        controllerDir = os.path.abspath(os.path.dirname(__file__))
        gsshaindexDir = os.path.abspath(os.path.dirname(controllerDir))
        publicDir = os.path.join(gsshaindexDir, 'public')
        publicUser = os.path.join(publicDir, str(user_id))
        newFileDir = os.path.join(publicUser, 'newFile')
        writeFile = os.path.join(newFileDir, new_name)
        zipPath = os.path.join(newFileDir, '.'.join((new_name,'zip')))

        # Clear workspace folders
        gi_lib.clear_folder(newFileDir)
        gi_lib.clear_folder(writeFile)
        
        # Get all the project files
        projectFileAll.writeInput(session=gsshapy_session, directory=writeFile, name=new_name)
        
        # Make a list of the project files
        writeFile_list = os.listdir(writeFile)
        
        # Add each project file to the zip folder
        with zipfile.ZipFile(zipPath, "w") as gssha_zip:
            for item in writeFile_list:
                abs_path = path.join(writeFile, item)
                archive_path = path.join(new_name, item)
                gssha_zip.write(abs_path, archive_path)
        
        # get the url for uploading
        url = config["ckan.site_url"]
        if (url[:-1] == '/'):
            url += 'api'
        else:
            url += '/api'
        
        # Add the zipped GSSHA file to the public ckan
        results, success = gi_lib.add_zip_GSSHA(user_id, c.password, c, zipPath, new_name, new_description, pretty_date, fileName=new_name)
        
        # If the file zips correctly, get information and store it in the database
        if success == True:
            new_url = results['url']
            new_name = results['name']
            original_url = job.original_model['url']
            original_name = job.original_model['name']
        
        print "Original URL", original_url
        print "New URL", new_url

        model_data = {'original': {'url':original_url, 'name':original_name}, 'new':{'url':new_url, 'name':new_name}}
        job.run_urls = model_data
        job.new_name = new_name
        session.commit()
        
        redirect(t.url_for('gsshaindex-status'))
    
    def status(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Find user id
        user_id = c.author or c.user
        
        # Get the jobs from the database
        session = jobs_sessionmaker()
        c.job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        order_by(Jobs.created.desc()).all()
        
        # Create array of jobs. If they haven't been submitted (new_name = None), don't add it to the list.
        c.job_info=[]               
        for job in c.job:
            if job.new_name == None:
                break
            else:
                info=[job.new_name, job.status, job.id]
                c.job_info.append(info)
        
        return t.render('gsshaindex/zipped.html')
    
    @jsonify
    def fly(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        print "Fly GSSHA, fly!!!"
        
        # Get user id and job id
        user_id = c.author or c.user
        job_id = c.job_id
        
        # Specify workspaces
        controllerDir = os.path.abspath(os.path.dirname(__file__))
        gsshaindexDir = os.path.abspath(os.path.dirname(controllerDir))
        publicDir = os.path.join(gsshaindexDir, 'public')
        publicUser = os.path.join(publicDir, str(user_id))
        resultsPath = os.path.join(publicUser, "results")
        
        # Clear the results folder
        gi_lib.clear_folder(resultsPath)
        
        # Get the job from the database
        session = jobs_sessionmaker()
        job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
        
        # Get the urls and names for the analysis
        run_urls = job.run_urls   
                     
        arguments={'new': {'url':run_urls['new']['url'], 'name':run_urls['new']['name']}, 'original':{'url':run_urls['original']['url'], 'name':run_urls['original']['name']}}
        
        # Set up for fly GSSHA
        job.status = "processing"
        session.commit() 
        
        status = 'complete'      
              
        results = []
        results_urls = []
        count = 0

        # Try running the web service
        try: 
            for k in arguments:
                url = str(arguments[k]['url'])
                resultsFile = os.path.join(resultsPath, arguments[k]['name'].replace(" ","_")+datetime.now().strftime('%Y%d%m%H%M%S'))
                gi_lib.flyGssha(url, resultsFile)

                # Push file to ckan dataset
                api_base_location = '/'.join((config['ckan.site_url'],'api'))
                resource_name = ' '.join((user_id, 'GSSHA Run', datetime.now().strftime('%b %d %y %H:%M:%S')))
                add_file_to_package(base_location=str(api_base_location),
                                    context=c,
                                    package_name='gssha-runner',
                                    file_path=resultsFile,
                                    name=resource_name, 
                                    format='zip', 
                                    model='GSSHA')
                
                # Publish link to table
                result = get_resource_by_field_value('name:' + resource_name)
                results.append(result)
                count +=1
            
            if (len(results) == 2):
                results_urls = [results[0]['results'][0]['url'], results[1]['results'][0]['url']]
            else:
                status = 'failed'
                     
        except:
            status = 'failed'
                
        job.status = status
        job.result_urls = results_urls
        session.commit()
            
#         return {'status': status, 'links': results_urls}
        redirect(t.url_for('gsshaindex-status'))
    
    def delete(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Get user id and job id
        user_id = c.author or c.user
        job_id = c.job_id
        
        # Get the job from the database and delete
        session = jobs_sessionmaker()
        job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
        session.delete(job)
        session.commit()
        
        redirect(t.url_for('gsshaindex-status'))
    
    def results(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Get user id and job id
        user_id = c.author or c.user
        job_id = c.job_id
        
        # Get the job from the database
        session = jobs_sessionmaker()
        job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
        
        # Get the run result urls
        result_files = job.result_urls
        
        # Specify workspaces
        controllerDir = os.path.abspath(os.path.dirname(__file__))
        gsshaindexDir = os.path.abspath(os.path.dirname(controllerDir))
        publicDir = os.path.join(gsshaindexDir, 'public')
        publicUser = os.path.join(publicDir, str(user_id))
        resultsPath = os.path.join(publicUser, "results")
        
        # Clear the results folder
        gi_lib.clear_folder(publicUser)
        gi_lib.clear_folder(resultsPath)
        
        # Get the otl files
        otl_files = []
        for url in result_files:
            print url
            otl_file = gi_lib.extract_otl(url, resultsPath)
            otl_files.append(otl_file)
        
        # Format the values for display with high charts
        new_values = []
        originalValues = []
        count = 0
        for result_location in otl_files:
            if count==0:
                newFileDir = os.path.join(resultsPath, result_location)
                with open(newFileDir, 'r') as f:
                    values = [row.strip().split('   ') for row in f]
                for thing in values:
                    formatted_value = []
                    for item in thing:
                        item = float(item)
                        formatted_value.append(item)
                    new_values.append(formatted_value)
                count +=1
            else:
                originalFileDir = os.path.join(resultsPath,result_location)
                print originalFileDir
                with open(originalFileDir, 'r') as f:
                    values = [row.strip().split('   ') for row in f]
                for thing in values:
                    formatted_value = []
                    for item in thing:
                        item = float(item)
                        formatted_value.append(item)
                    originalValues.append(formatted_value)
        
        # Set up for high charts hydrograph
        highcharts_object = {
                'chart': {
                    'type': 'spline'
                },
                'title': {
                    'text': 'Comparison Hydrograph'
                },
                'subtitle': {
                    'text': 'Display of the two model results'
                },
                'legend': {
                    'layout': 'vertical',
                    'align': 'right',
                    'verticalAlign': 'middle',
                    'borderWidth': 0
                },
                'xAxis': {
                    'title': {
                        'enabled': True,
                        'text': 'Time (hours)'
                    },
                    'labels': {
                        'formatter': 'function () { return this.value + " hr"; }'
                    }
                },
                'yAxis': {
                    'title': {
                        'enabled': True,
                        'text': 'Discharge (cfs)'
                    },
                    'labels': {
                        'formatter': 'function () { return this.value + " cfs"; }'
                    }
                },
                'tooltip': {
                    'headerFormat': '{series.name}',
                    'pointFormat': '{point.x} hours: {point.y} cfs'
                 },
                'series': [{
                    'name': job.name.replace("_", " "),
                    'color': '#0066ff',
                    'dashStyle': 'ShortDash',
                    'marker' : {'enabled': False},
                    'data': originalValues
                    },{
                    'name': job.new_name.replace("_", " "),
                    'marker' : {'enabled': False},
                    'color': '#ff6600',
                    'data': new_values}
                ]}
            
        c.hydrograph = {'highcharts_object': highcharts_object,
                            'width': '500px',
                            'height': '500px'} 
         
        return t.render('gsshaindex/results.html')
