from ckan.lib.base import BaseController
import ckan.plugins as p
from ckanext.tethys_apps.lib.dataset_tools import *
from ..models import Jobs, jobs_sessionmaker, gsshapy_sessionmaker, gsshapy_engine
import os
import pylons.config as config
import subprocess
import json
import time
from mapkit.RasterConverter import RasterConverter
from ckanext.tethys_apps.lib import get_app_definition, set_session_global, set_session_globals, get_session_global
import ckanapp.gsshaindex.lib as gi_lib  
from gsshapy.orm import ProjectFile, IndexMap, RasterMapFile, MTIndex, MTValue, MapTableFile, MapTable
import ckanapp.gsshaindex.lib as gi_lib
from pylons.controllers.util import redirect
from sqlalchemy import distinct
import ConfigParser
import operator
import pprint
from ckanext.tethys_apps.lib.dataset_tools import *
from mapkit import Base
from pylons.decorators import jsonify
import ckanapp.gsshaindex.lib.geoserver as geoserver 
import xml.etree.ElementTree as ET
import StringIO
from xml.dom import minidom

from ..models import Jobs, jobs_sessionmaker, gsshapy_sessionmaker, shapefile_sessionmaker, shapefile_engine

# Get app.ini
controller_dir = os.path.abspath(os.path.dirname(__file__))
config_path = os.path.join(os.path.abspath(os.path.dirname(controller_dir)), 'app.ini')
app_config = ConfigParser.RawConfigParser()
app_config.read(config_path)

maps_api_key = app_config.get('api_key', 'maps_api_key')
shp2pgsql = app_config.get('postgis', 'shp2pgsql_path')
    
class ShapefileController(BaseController):    
    def shapefile_index(self):        
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Get the job id
        job_id = c.job_id
        shapefile_id = c.shapefile_id
        
        # Find user id and password
        user_id = c.author or c.user
        password = c.password
        
        # Get the job from the database
        job_session = jobs_sessionmaker()
        job = job_session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
                        
        # Create gsshapy_session
        gsshapy_session = gsshapy_sessionmaker()
        
        # Create shapefile session
        shapefile_session = shapefile_sessionmaker()
        
        # Get project file id                
        project_file_id = job.new_model['project_file_id']
  
        # Get list of shapefiles
        shapefile_list = get_resource_by_field_value('model:Shapefile')
        pprint.pprint(shapefile_list)
        
        c.shapefile_list = []
        
        # Fill the array with information on the GSSHA models
        if shapefile_list.get('count') > 0:
            for result in shapefile_list.get('results'):
                if result.get('description')=="":
                    description = "None"
                else:
                    description = result.get('description')
                c.shapefile_list.append({"name":result.get('name'), "id":result.get('id'), "description":description, "url":result.get('url')})
                
        c.shapefile_list.sort(key=operator.itemgetter('name')) 
        
        for shapefile in c.shapefile_list:
            if shapefile['id'] == shapefile_id:
                c.file_id = shapefile['id']
                c.file_name = shapefile['name']
                c.file_url = shapefile['url']
         
        # Specify workspace
        controllerDir = os.path.abspath(os.path.dirname(__file__))
        gsshaindexDir = os.path.abspath(os.path.dirname(controllerDir))
        publicDir = os.path.join(gsshaindexDir, 'public')
        publicUser = os.path.join(publicDir, str(user_id))
        indexMapDir = os.path.join(publicUser, 'index_maps')
          
        # Use project id to link to original map table file
        project_file = gsshapy_session.query(ProjectFile).filter(ProjectFile.id == project_file_id).one()
        new_index = gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == c.map_name).one()
        c.mapTables = new_index.mapTables
        
        # Get list of index files
        resource_list = json.loads(job.current_kmls)
        
        c.resource_name = []
        c.resource_url = []
        # Get array of names and urls
        for key in resource_list:
            c.resource_name.append(key)

        # Create kml file name and path
        current_time = time.strftime("%Y%m%dT%H%M%S")
        resource_name = new_index.name + "_" + str(user_id) + "_" + current_time
        kml_ext = resource_name + '.kml'
        clusterFile = os.path.join(indexMapDir, kml_ext)

        file_present = False
        for key in resource_list:
            if key == c.map_name:
                file_present = True
        
        if file_present == False:
            # Generate color ramp
            new_index.getAsKmlClusters(job_session=gsshapy_session,
                                           path = clusterFile,
                                           colorRamp = RasterConverter.COLOR_RAMP_HUE,
                                           alpha=1.0)
            
            result, result['resources'] = gi_lib.add_kml_CKAN(user_id, password, clusterFile, resource_name, new_index.name)
                
            for resource in result['resources']:
                if resource['name'] == resource_name:
                    c.resource_list[new_index.name] = {'url':resource['url'], 'full_name':resource['name']}
                    break
            
            job.current_kmls = json.dumps(c.resource_list)
            
            job_session.commit()
            
        
        # Create an array of the projections for the selet_projection modal
        projection_query = '''SELECT srid, srtext FROM spatial_ref_sys'''
        
        result_prj_query = gsshapy_engine.execute(projection_query)
        
        c.projection_list = []
        
        for row in result_prj_query:
            srid = row['srid']
            short_desc = row['srtext'].split('"')[1]
            c.projection_list.append({"srid":srid, "short_desc":short_desc})
            
            
#         ###### THIS IS THE CODE THAT GETS THE OVERLAYS ###### IT MAY BE MOVED TO A "SWAP_GEOJSON" FUNCTION LATER ####
# 
#         # Specify workspace
#         workspace = os.path.join(gsshaindexDir,'workspace')
#         userDir = os.path.join(workspace, str(user_id))
#         
#         # Clear the workspace
#         gi_lib.clear_folder(userDir)
#         
#         # Get the overlay to display
#         for result in shapefile_list.get('results'):
#             if result.get('id')==c.shapefile_id:
#                 url = result.get('url')
#                 name = result.get('name')
#                 norm_name = name.replace(" ","")
#                 
#                 extract_path, unique_dir = extract_zip_from_url(user_id, url, userDir)
#                 for root, dirs, files in os.walk(extract_path):
#                     for file in files:
#                         if "." in file:
#                             new_name = str(user_id)+file[-4:]
#                             os.rename(os.path.join(root,file), os.path.join(root,new_name))
#                             if file.endswith(".dbf"):
#                                 dbf_path = os.path.join(root,new_name)
#                                 
#                 #Try deleting that table name
#                 delete_statement = '''DROP TABLE '''+ user_id +''';'''
#                 try:
#                     shapefile_engine.execute(delete_statement)
#                 except:
#                     pass
#         
#                 # Write statement that will create table for shapefile in the database
#                 shapefile2pgsql = subprocess.Popen([shp2pgsql,
#                                                     str(dbf_path)],
#                                                    stdout=subprocess.PIPE)
#                 
#                 if project_file.srid == None:
#                     srid = 26912
#                 else:
#                     srid = project_file.srid
#                  
#                 # Check to see if there are errors or if it worked
#                 sql, error = shapefile2pgsql.communicate()
#                 print "ERROR:", error
#                 if error == None:
#                     result = shapefile_engine.execute(sql)
#                     
#                     get_wkt = ''' SELECT gid, ST_AsText(ST_Transform(ST_SetSRID(foo.geom,{1}),4326)) AS geoms
#                                   FROM (SELECT (ST_Dump(geom)).geom as geom, gid as gid
#                                   FROM {0}) as foo; '''.format(user_id,srid)
#                     
# #                     get_wkt = ''' SELECT gid, ST_AsText(foo.geom) geoms
# #                                  FROM (SELECT (ST_Dump(geom)).geom as geom, gid as gid
# #                                  FROM {0}) as foo; '''.format(user_id)
# 
#                     result1 = shapefile_engine.execute(get_wkt)
#                     
#                     wkt_geoms = []
#                     
#                     for row in result1:
#                         coord = str(row['geoms'])
#                         type = str(row['geoms'].partition("(")[0])
#                         type = type.title()
#                         if type == 'Linestring':
#                             type = 'PolyLine'
#                          
#                         wkt_geom = {"type": type,
#                                          "wkt":coord,
#                                          "properties":{"value":row['gid']}}
#                         wkt_geoms.append(wkt_geom)
#                         
# 
#                     wkt_json = {'type': 'WKTGeometryCollection',
#                                 'geometries': wkt_geoms}
#                     
#                     pprint.pprint(wkt_json)
    
        
        ######## END OF THE OVERLAY CODE #######
            

        
        c.editable_map = {'height': '600px',
                          'width': '100%', 
                          'reference_kml_action': '/apps/gssha-index/'+c.job_id +'/show-overlay/'+ c.map_name,
                          'maps_api_key': maps_api_key,
                          'output_format':'WKT'}
#                           'output_format': 'WKT',
#                           'input_overlays':wkt_json}
        my_ckan = config["ckan.site_url"]
        print my_ckan
    
        return t.render('gsshaindex/select_shapefile.html')
    
    def get_srid_from_wkt(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
           
        url = c.url
        url = "http://prj2epsg.org/search.json?mode=wkt&terms=" + url
        r= requests.get(url)
        status = r.status_code
        if status == 200:
            result =  r.json()
            prj_code =  str(result['codes'][0]['code'])
            return (prj_code)
        else:
            return ('error')
    
    def shapefile_upload(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Find user id
        user_id = c.author or c.user
        password = c.password
        
        # Specify workspace
        controllerDir = os.path.abspath(os.path.dirname(__file__))
        gsshaindexDir = os.path.abspath(os.path.dirname(controllerDir))
        workspace = os.path.join(gsshaindexDir,'workspace')
        userDir = os.path.join(workspace, str(user_id))
        
        # Clear the workspace
        gi_lib.clear_folder(userDir)
        
        filename = t.request.POST['shapefile_files'].filename
        input_file = t.request.POST['shapefile_files'].file
        
        # Get the params
        params = t.request.params
        print params
        name = params['shapefile_name']
        description = params['shapefile_description']
        srid = params['projection-number']
        # Reformat the name by removing bad characters
        bad_char = "',.<>()[]{}=+-/\"|:;\\^?!~`@#$%&* "
        for char in bad_char:
            new_name = name.replace(char,"_")
        zip_name = new_name + ".zip"
        zip_path = os.path.join(userDir, zip_name)
        
        shp_list = []

        for thing, value in params.iteritems():
            if thing == "shapefile_files":
                if value.filename.endswith('.shp'):
                    shp_list.append({value.filename: value.file})
                elif value.filename.endswith('.shx'):
                    shp_list.append({value.filename: value.file})
                elif value.filename.endswith('.dbf'):
                    shp_list.append({value.filename: value.file})
                elif value.filename.endswith('.prj'):
                    shp_list.append({value.filename: value.file})
                elif value.filename.endswith('.sbx'):
                    shp_list.append({value.filename: value.file})
                elif value.filename.endswith('.sbn'):
                    shp_list.append({value.filename: value.file})
                elif value.filename.endswith('.xml'):
                    shp_list.append({value.filename: value.file})

        temp_dir = os.path.join(userDir, new_name)
        os.mkdir(temp_dir)
        
        for thing in shp_list:
            file_path = os.path.join(temp_dir, thing.keys()[0])

            with open(file_path, 'w') as f:
                f.write(thing.values()[0].read())
        
        # Get list of files to be zipped    
        writeFile_list = os.listdir(temp_dir)  
        
        # Add files to the zip folder
        with zipfile.ZipFile(zip_path, "w") as shp_zip:
            for item in writeFile_list:
                abs_path = path.join(temp_dir, item)
                archive_path = path.join(new_name, item)
                shp_zip.write(abs_path, archive_path)
        
#         geoserver.create_geoserver_workspace('gsshaindex')
# #         geoserver.create_datastore('gsshaindex', user_id)        
#         print "This needs to happen for geoserver stuff"        
#         geoserver.create_layer("gsshaindex", user_id, zip_path, name)
        
#         result = gi_lib.append_shapefile_CKAN(c, zip_path, name, description, srid)
        print zip_path
        print name
        print description

        redirect(t.url_for('gsshaindex-shapefile-index', map_name=c.map_name, job_id=c.job_id, shapefile_id=c.shapefile_id))
        
    
    @jsonify
    def show_overlay(self):
        # This displays the shapefile
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Get the job id and user id
        job_id = c.job_id
        user_id = c.author or c.user
        password = c.password
        
        # Get the job from the database
        session = jobs_sessionmaker()
        job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
                        
        # Create gsshapy_session
        gsshapy_session = gsshapy_sessionmaker()
        
        # Get project file id                
        project_file_id = job.new_model['project_file_id']
                        
        # Specify workspace
        controllerDir = os.path.abspath(os.path.dirname(__file__))
        gsshaindexDir = os.path.abspath(os.path.dirname(controllerDir))
        
        # Get list of shapefiles
        shapefile_list = get_resource_by_field_value('model:Shapefile')
        
        project_file = gsshapy_session.query(ProjectFile).filter(ProjectFile.id == project_file_id).one()

        ###### THIS IS THE CODE THAT GETS THE OVERLAYS ###### IT MAY BE MOVED TO A "SWAP_GEOJSON" FUNCTION LATER ####

        # Specify workspace
        workspace = os.path.join(gsshaindexDir,'workspace')
        userDir = os.path.join(workspace, str(user_id))
        
        # Clear the workspace
        gi_lib.clear_folder(userDir)
        
        wkt_json = {'type': 'WKTGeometryCollection',
                                'geometries': ''}
        
        kml = ""
        
        # Get the overlay to display
        for result in shapefile_list.get('results'):
            if result.get('id')==c.shapefile_id:
                url = result.get('url')
                name = result.get('name')
                norm_name = name.replace(" ","")
                
                extract_path, unique_dir = extract_zip_from_url(user_id, url, userDir)
                for root, dirs, files in os.walk(extract_path):
                    for file in files:
                        if "." in file:
                            new_name = str(user_id)+file[-4:]
                            os.rename(os.path.join(root,file), os.path.join(root,new_name))
                            if file.endswith(".dbf"):
                                dbf_path = os.path.join(root,new_name)
                                
                #Try deleting that table name
                delete_statement = '''DROP TABLE '''+ user_id +''';'''
                try:
                    shapefile_engine.execute(delete_statement)
                except:
                    pass
        
                # Write statement that will create table for shapefile in the database
                shapefile2pgsql = subprocess.Popen([shp2pgsql,
                                                    str(dbf_path)],
                                                   stdout=subprocess.PIPE)
                
                if project_file.srid == None:
                    srid = 4326
#                     srid = 26912
                else:
                    srid = project_file.srid
                 
                # Check to see if there are errors or if it worked
                sql, error = shapefile2pgsql.communicate()
                print "ERROR:", error
                
                if error == None:
                    result = shapefile_engine.execute(sql)
                    
                    get_wkt = ''' SELECT gid, ST_AsKML(ST_Transform(ST_SetSRID(foo.geom,{1}),4326)) AS geoms
                                  FROM (SELECT (ST_Dump(geom)).geom as geom, gid as gid
                                  FROM {0}) as foo; '''.format(user_id,srid)

                    get_kml = ''' SELECT gid, ST_AsKML(ST_Transform(ST_SetSRID(foo.geom,{1}),4326)) AS geoms
                                  FROM (SELECT (ST_Dump(geom)).geom as geom, gid as gid
                                  FROM {0}) as foo; '''.format(user_id,srid)

                    result1 = shapefile_engine.execute(get_kml)
                    
                    wkt_geoms = []
                    
                    ### Mimic map's process for making kmls ###
                    
                    # Initialize KML Document
                    kml = ET.Element('kml', xmlns='http://www.opengis.net/kml/2.2')
                    document = ET.SubElement(kml, 'Document')
                    docName = ET.SubElement(document, 'name')
                    docName.text = project_file.name
                    
                    groupValue = -9999999.0
                    
                    for row in result1:
#                         coord = str(row['geoms'])
#                         print coord
                        
                        if (row.gid):
                            value = float(row.gid)
                        else:
                            value = None
                            
                        polygonString = row.geoms
                        
                        if (value):
                            if (value != groupValue):
                                placemark = ET.SubElement(document, 'Placemark')
                                placemarkName = ET.SubElement(placemark, 'name')
                                placemarkName.text = str(value)
                                
                                multigeometry = ET.SubElement(placemark, 'MultiGeometry')
                                
                                extendedData = ET.SubElement(placemark, 'MultiGeometry')
                                
                                valueData = ET.SubElement(extendedData, 'Data', name='value')
                                valueValue = ET.SubElement(valueData, 'value')
                                valueValue.text = str(value)
                                
                                groupValue = value
                            
                            polygon = ET.fromstring(polygonString)
                            multigeometry.append(polygon)
                            

        kml_string = minidom.parseString(ET.tostring(kml))
        kml_file = open(os.path.join(userDir, 'kml_file_name.kml'),"w")
        kml_file.write(kml_string.toprettyxml(indent="   "))
        kml_file.close()
#         return ET.tostring(kml)
                            
#                         type = str(row['geoms'].partition("(")[0])
#                         type = type.title()
#                         if type == 'Linestring':
#                             type = 'PolyLine'
#                          
#                         wkt_geom = {"type": type,
#                                          "wkt":coord,
#                                          "properties":{"value":row['gid']}}
#                         wkt_geoms.append(wkt_geom)
#                         
# 
#                     wkt_json = {'type': 'WKTGeometryCollection',
#                                 'geometries': wkt_geoms}
                    
#                     pprint.pprint(wkt_json)

        
#         return{'overlay_json': wkt_json}

    def submit_edits(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Get geometry information
        params = t.request.params
                
        if params['geometry']== "":
            redirect(t.url_for('gsshaindex-edit-index', map_name=c.map_name, job_id=c.job_id))
        
        # Get job id
        job_id = c.job_id
        
        # Find user id and password
        user_id = c.author or c.user
        password = c.password
        
        # Get the job from the database
        session = jobs_sessionmaker()
        job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
        
        # Get project file id                
        project_file_id = job.new_model['project_file_id']
        
        #Create session
        gsshapy_session = gsshapy_sessionmaker()
        
        # Use project id to link to original map table file
        project_file = gsshapy_session.query(ProjectFile).filter(ProjectFile.id == project_file_id).one()
        index_raster = gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == c.map_name).one()
        mask_file = gsshapy_session.query(RasterMapFile).filter(RasterMapFile.projectFileID == project_file_id).filter(RasterMapFile.fileExtension == "msk").one()

        mapTables = index_raster.mapTables
        
        jsonGeom = json.loads(params['geometry'])
        geometries= jsonGeom['geometries']
        
        #Convert from json to WKT
        for geometry in geometries:
            wkt = geometry['wkt']
            
            value = geometry['properties']['value']
            print "WKT: ", value
            
            #Loop through indices and see if they match
            index_raster_indices = index_raster.indices
            index_present = False
            for index in index_raster_indices:
                if int(index.index) == int(value):
                    index_present = True
                    break
            
            # Create new index value if it doesn't exist and add change the number of ids        
            if index_present == False:       
                new_indice = MTIndex(value, "", "")
                new_indice.indexMap = index_raster
                for mapping_table in mapTables:
                    print "Mapping Table:", mapping_table
                    distinct_vars = gsshapy_session.query(distinct(MTValue.variable)).\
                                     filter(MTValue.mapTable == mapping_table).\
                                     order_by(MTValue.variable).\
                                     all()
                    print "Distinct Vars: ", distinct_vars                
                    variables = []
                    for var in distinct_vars:
                        variables.append(var[0])
                        
                    for variable in variables:
                        print variable
                        new_value = MTValue(variable, 0)
                        new_value.mapTable = mapping_table
                        new_value.index = new_indice
                   
                    
                gsshapy_session.commit()   
            print "id1: ", index_raster.id
            # Change values in the index map
            statement = '''UPDATE idx_index_maps
                            SET raster = ST_SetValue(raster,1, ST_Transform(ST_GeomFromText(\''''+ unicode(wkt) +'''\',4326),'''+ unicode(project_file.srid) +'''),'''+ unicode(value) +''')
                            WHERE id = '''+ unicode(index_raster.id) +''';
                            '''    
            result = gsshapy_engine.execute(statement) 
            
            gsshapy_session.commit() 
        
        print "id: ", index_raster.id     
        # Crop the index map by the mask map        
        statementclip = ''' UPDATE idx_index_maps
                            SET raster = ST_MapAlgebra(
                            (SELECT raster FROM idx_index_maps WHERE id = '''+ unicode(index_raster.id) +'''),1,
                            (SELECT raster FROM idx_index_maps WHERE id = '''+ unicode(mask_file.id) +'''),1,
                            '([rast1]*[rast2])'
                            )
                            WHERE id = '''+ unicode(index_raster.id) +''';
                        '''
        
#         statement2 = '''UPDATE idx_index_maps
#                         Set raster = ST_Clip(raster, 1, (SELECT geom As polygon
#                         FROM (SELECT (ST_DumpAsPolygons(raster)).*
#                         FROM raster_maps WHERE id = '''+ unicode(mask_file.id) +''')
#                         As foo
#                         ORDER By val), TRUE)
#                         WHERE id = '''+ unicode(index_raster.id) +''';
#                         '''
        # Get the values in the index map
        statement3 = '''SELECT (pvc).*
                        FROM (SELECT ST_ValueCount(raster,1,true) As pvc
                        FROM idx_index_maps WHERE id = '''+ unicode(index_raster.id) +''') AS foo
                        ORDER BY (pvc).value;
                        '''
#         result2 = gsshapy_engine.execute(statementclip) 
        result3 = gsshapy_engine.execute(statement3) 
        
        numberIDs = 0
        ids = []
        for row in result3:
            numberIDs +=1
            ids.append(row.value)
   
#         index_raster.mapTables[mapTableNumber].numIDs = numberIDs
#         gsshapy_session.commit()
        
        map_table_count = 0
        for mapping_table in mapTables:
            
            index_raster.mapTables[map_table_count].numIDs = numberIDs
            gsshapy_session.commit()
            
            indices = gsshapy_session.query(distinct(MTIndex.index), MTIndex.id, MTIndex.description1, MTIndex.description2).\
                                   join(MTValue).\
                                   filter(MTValue.mapTable == mapTables[map_table_count]).\
                                   order_by(MTIndex.index).\
                                   all()
    
            for index in indices:
                if not int(index[0]) in ids:
                    bob = gsshapy_session.query(MTIndex).get(index.id)
                    for val in bob.values:
                        gsshapy_session.delete(val)
                    gsshapy_session.delete(bob)
            
            gsshapy_session.commit()
            map_table_count +=1
            
        print ids
            
        index_raster =  gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == c.map_name).one()
        
        # Specify workspace
        controllerDir = os.path.abspath(os.path.dirname(__file__))
        gsshaindexDir = os.path.abspath(os.path.dirname(controllerDir))
        publicDir = os.path.join(gsshaindexDir, 'public')
        publicUser = os.path.join(publicDir, str(user_id))
        indexMapDir = os.path.join(publicUser, 'index_maps')
        
        # Create kml file name and path
        current_time = time.strftime("%Y%m%dT%H%M%S")
        resource_name = index_raster.name + "_" + str(user_id) + "_" + current_time
        kml_ext = resource_name + '.kml'
        clusterFile = os.path.join(indexMapDir, kml_ext)
        
        # Generate color ramp
        index_raster.getAsKmlClusters(session=gsshapy_session,
                                       path = clusterFile,
                                       colorRamp = RasterConverter.COLOR_RAMP_HUE,
                                       alpha=1.0)
        
        resource, status = gi_lib.add_kml_CKAN(user_id, password, c, clusterFile, resource_name, index_raster.name)
        
        temp_list = json.loads(job.current_kmls)  
        
        if status == True:
            for item in temp_list:
                    if item == c.map_name:
                        del temp_list[item]
                        temp_list[c.map_name] = {'url':resource['url'], 'full_name':resource['name']}
                        break

        job.current_kmls = json.dumps(temp_list)
        session.commit()
                 
        redirect(t.url_for('gsshaindex-edit-index', map_name=c.map_name, job_id=c.job_id))
 