from ckan.lib.base import BaseController
import ckan.plugins as p
from ..models import Jobs, jobs_sessionmaker, gsshapy_sessionmaker, gsshapy_engine
import os
import json
import time
from mapkit.RasterConverter import RasterConverter
from gsshapy.orm import ProjectFile, IndexMap, RasterMapFile, MTIndex, MTValue, MapTableFile, MapTable
import ckanapp.gsshaindex.lib as gi_lib
from pylons.controllers.util import redirect
from sqlalchemy import distinct
import ConfigParser

# Get app.ini
controller_dir = os.path.abspath(os.path.dirname(__file__))
config_path = os.path.join(os.path.abspath(os.path.dirname(controller_dir)), 'app.ini')
app_config = ConfigParser.RawConfigParser()
app_config.read(config_path)

maps_api_key = app_config.get('api_key', 'maps_api_key')
    
class DrawController(BaseController):    
    def edit_index(self):        
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Get the job id
        job_id = c.job_id
        
        # Find user id and password
        user_id = c.author or c.user
        password = c.password
        
        # Get the job from the database
        job_session = jobs_sessionmaker()
        job = job_session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
        
        # Get project file id                
        project_file_id = job.new_model['project_file_id']
         
        # Specify workspace
        controllerDir = os.path.abspath(os.path.dirname(__file__))
        gsshaindexDir = os.path.abspath(os.path.dirname(controllerDir))
        publicDir = os.path.join(gsshaindexDir, 'public')
        publicUser = os.path.join(publicDir, str(user_id))
        indexMapDir = os.path.join(publicUser, 'index_maps')
         
        # Create job_session
        gsshapy_session = gsshapy_sessionmaker()
        
        # Use project id to link to original map table file
        project_file = gsshapy_session.query(ProjectFile).filter(ProjectFile.id == project_file_id).one()
        new_index = gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == c.map_name).one()
        c.mapTables = new_index.mapTables
        c.indices = new_index.indices
    
        
        # Get list of index files
        resource_list = json.loads(job.current_kmls)
        
        c.resource_name = []
        c.resource_url = []
        # Get array of names and urls
        for key in resource_list:
            c.resource_name.append(key)

        # Create kml file name and path
        current_time = time.strftime("%Y%m%dT%H%M%S")
        resource_name = new_index.name + "_" + str(user_id) + "_" + current_time
        kml_ext = resource_name + '.kml'
        clusterFile = os.path.join(indexMapDir, kml_ext)

        file_present = False
        for key in resource_list:
            if key == c.map_name:
                file_present = True
        
        if file_present == False:
            # Generate color ramp
            new_index.getAsKmlClusters(job_session=gsshapy_session,
                                           path = clusterFile,
                                           colorRamp = RasterConverter.COLOR_RAMP_HUE,
                                           alpha=1.0)
            
            result, result['resources'] = gi_lib.add_kml_CKAN(user_id, password, clusterFile, resource_name, new_index.name)
                
            for resource in result['resources']:
                if resource['name'] == resource_name:
                    c.resource_list[new_index.name] = {'url':resource['url'], 'full_name':resource['name']}
                    break
            
            job.current_kmls = json.dumps(c.resource_list)
            
            job_session.commit()
        
        c.editable_map = {'height': '600px',
                          'width': '100%', 
                          'reference_kml_action': '/apps/gssha-index/get-index-maps/'+c.job_id +'/'+ c.map_name,
                          'maps_api_key': maps_api_key,
                          'drawing_types_enabled': ['POLYGONS'],
                          'initial_drawing_mode': 'POLYGONS',
                          'output_format': 'WKT'}
        
        return t.render('gsshaindex/edit_index.html')
    
    def submit_edits(self):
        # Tools
        t = p.toolkit
        c = t.c
        _ = t._
        
        # Get geometry information
        params = t.request.params
                
        if params['geometry']== "":
            redirect(t.url_for('gsshaindex-edit-index', map_name=c.map_name, job_id=c.job_id))
        
        # Get job id
        job_id = c.job_id
        
        # Find user id and password
        user_id = c.author or c.user
        password = c.password
        
        # Get the job from the database
        session = jobs_sessionmaker()
        job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        filter(Jobs.id == job_id).one()
        
        # Get project file id                
        project_file_id = job.new_model['project_file_id']
        
        #Create session
        gsshapy_session = gsshapy_sessionmaker()
        
        # Use project id to link to original map table file
        project_file = gsshapy_session.query(ProjectFile).filter(ProjectFile.id == project_file_id).one()
        index_raster = gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == c.map_name).one()
        mask_file = gsshapy_session.query(RasterMapFile).filter(RasterMapFile.projectFileID == project_file_id).filter(RasterMapFile.fileExtension == "msk").one()

        mapTables = index_raster.mapTables
        
        jsonGeom = json.loads(params['geometry'])
        geometries= jsonGeom['geometries']
        
        #Convert from json to WKT
        for geometry in geometries:
            wkt = geometry['wkt']
            
            value = geometry['properties']['value']
            print "WKT: ", value
            
            #Loop through indices and see if they match
            index_raster_indices = index_raster.indices
            index_present = False
            for index in index_raster_indices:
                if int(index.index) == int(value):
                    index_present = True
                    break
            
            # Create new index value if it doesn't exist and add change the number of ids        
            if index_present == False:       
                new_indice = MTIndex(value, "", "")
                new_indice.indexMap = index_raster
                for mapping_table in mapTables:
                    print "Mapping Table:", mapping_table
                    distinct_vars = gsshapy_session.query(distinct(MTValue.variable)).\
                                     filter(MTValue.mapTable == mapping_table).\
                                     order_by(MTValue.variable).\
                                     all()
                    print "Distinct Vars: ", distinct_vars                
                    variables = []
                    for var in distinct_vars:
                        variables.append(var[0])
                        
                    for variable in variables:
                        print variable
                        new_value = MTValue(variable, 0)
                        new_value.mapTable = mapping_table
                        new_value.index = new_indice
                        
            if project_file.srid == None:
                srid = 26912
            else:
                srid = project_file.srid
                    
                gsshapy_session.commit()   
            print "id1: ", index_raster.id
            # Change values in the index map
            statement = '''UPDATE idx_index_maps
                            SET raster = ST_SetValue(raster,1, ST_Transform(ST_GeomFromText(\''''+ unicode(wkt) +'''\',4326),'''+ unicode(srid) +'''),'''+ unicode(value) +''')
                            WHERE id = '''+ unicode(index_raster.id) +''';
                            '''    
            result = gsshapy_engine.execute(statement) 
            
            gsshapy_session.commit() 
        
        print "id: ", index_raster.id     
        # Crop the index map by the mask map        
        statementclip = ''' UPDATE idx_index_maps
                            SET raster = ST_MapAlgebra(
                            (SELECT raster FROM idx_index_maps WHERE id = '''+ unicode(index_raster.id) +'''),1,
                            (SELECT raster FROM idx_index_maps WHERE id = '''+ unicode(mask_file.id) +'''),1,
                            '([rast1]*[rast2])'
                            )
                            WHERE id = '''+ unicode(index_raster.id) +''';
                        '''
        
#         statement2 = '''UPDATE idx_index_maps
#                         Set raster = ST_Clip(raster, 1, (SELECT geom As polygon
#                         FROM (SELECT (ST_DumpAsPolygons(raster)).*
#                         FROM raster_maps WHERE id = '''+ unicode(mask_file.id) +''')
#                         As foo
#                         ORDER By val), TRUE)
#                         WHERE id = '''+ unicode(index_raster.id) +''';
#                         '''
        # Get the values in the index map
        statement3 = '''SELECT (pvc).*
                        FROM (SELECT ST_ValueCount(raster,1,true) As pvc
                        FROM idx_index_maps WHERE id = '''+ unicode(index_raster.id) +''') AS foo
                        ORDER BY (pvc).value;
                        '''
#         result2 = gsshapy_engine.execute(statementclip) 
        result3 = gsshapy_engine.execute(statement3) 
        
        numberIDs = 0
        ids = []
        for row in result3:
            numberIDs +=1
            ids.append(row.value)
   
#         index_raster.mapTables[mapTableNumber].numIDs = numberIDs
#         gsshapy_session.commit()
        
        map_table_count = 0
        for mapping_table in mapTables:
            
            index_raster.mapTables[map_table_count].numIDs = numberIDs
            gsshapy_session.commit()
            
            indices = gsshapy_session.query(distinct(MTIndex.index), MTIndex.id, MTIndex.description1, MTIndex.description2).\
                                   join(MTValue).\
                                   filter(MTValue.mapTable == mapTables[map_table_count]).\
                                   order_by(MTIndex.index).\
                                   all()
    
            for index in indices:
                if not int(index[0]) in ids:
                    bob = gsshapy_session.query(MTIndex).get(index.id)
                    for val in bob.values:
                        gsshapy_session.delete(val)
                    gsshapy_session.delete(bob)
            
            gsshapy_session.commit()
            map_table_count +=1
            
        print ids
            
        index_raster =  gsshapy_session.query(IndexMap).filter(IndexMap.mapTableFile == project_file.mapTableFile).filter(IndexMap.name == c.map_name).one()
        
        # Specify workspace
        controllerDir = os.path.abspath(os.path.dirname(__file__))
        gsshaindexDir = os.path.abspath(os.path.dirname(controllerDir))
        publicDir = os.path.join(gsshaindexDir, 'public')
        publicUser = os.path.join(publicDir, str(user_id))
        indexMapDir = os.path.join(publicUser, 'index_maps')
        
        # Create kml file name and path
        current_time = time.strftime("%Y%m%dT%H%M%S")
        resource_name = index_raster.name + "_" + str(user_id) + "_" + current_time
        kml_ext = resource_name + '.kml'
        clusterFile = os.path.join(indexMapDir, kml_ext)
        
        # Generate color ramp
        index_raster.getAsKmlClusters(session=gsshapy_session,
                                       path = clusterFile,
                                       colorRamp = RasterConverter.COLOR_RAMP_HUE,
                                       alpha=1.0)
        
        resource, status = gi_lib.add_kml_CKAN(user_id, password, c, clusterFile, resource_name, index_raster.name)
        
        temp_list = json.loads(job.current_kmls)  
        
        if status == True:
            for item in temp_list:
                    if item == c.map_name:
                        del temp_list[item]
                        temp_list[c.map_name] = {'url':resource['url'], 'full_name':resource['name']}
                        break

        job.current_kmls = json.dumps(temp_list)
        session.commit()
                 
        redirect(t.url_for('gsshaindex-edit-index', map_name=c.map_name, job_id=c.job_id))
 