from ckan.lib.base import BaseController, h
import ckan.plugins as p

from ..models import Jobs, SessionMaker

class AjaxController(BaseController):
    
    def load_jobs(self):
        # Tools
        t= p.toolkit
        c = t.c
        
        # Get user id
        user_id = c.userobj.id
        
        # Get the jobs from the database
        session = SessionMaker()
        c.job = session.query(Jobs).\
                        filter(Jobs.user_id == user_id).\
                        order_by(Jobs.created.desc()).all()
                        
        return t.render('ckanapp/gsshaindex/ajax/jobs_table_chunk.html')
                        
        
        
