from ckanext.tethys_apps.lib.app_base import AppBase

class GSSHAIndexApp(AppBase):
    '''
    Example implementation of an app (this is the initializer for the app)
    '''
    
    def registerApp(self, app):
        '''
        Register the app
        '''
        
        app.addApp(name='GSSHA Index Map Editor',
                   index='gsshaindex',
                   icon='gsshaindex/images/luedit.jpg')
        
        
    def registerControllers(self, controllers):
        '''
        Add controllers
        '''
        
        controllers.addController(name='gsshaindex',
                                  url='gssha-index',
                                  controller='gsshaindex.controllers.index:GSSHAIndexController',
                                  action='index')

        controllers.addController(name='gsshaindex-extract-gssha',
                                  url='gssha-index/{job_id}/extract-gssha',
                                  controller='gsshaindex.controllers.index:GSSHAIndexController',
                                  action='extract_gssha')
              
        controllers.addController(name='gsshaindex-get-mask-map',
                                  url='gssha-index/get-mask-map/{file_id}',
                                  controller='gsshaindex.controllers.index:GSSHAIndexController',
                                  action='get_mask_map')  
        
        controllers.addController(name='gsshaindex-select-index',
                                  url='gssha-index/{job_id}/select-index',
                                  controller='gsshaindex.controllers.index:GSSHAIndexController',
                                  action='select_index')
        
        controllers.addController(name='gsshaindex-get-index-maps',
                                  url='gssha-index/get-index-maps/{job_id}/{map_name}',
                                  controller='gsshaindex.controllers.index:GSSHAIndexController',
                                  action='get_index_maps')

        controllers.addController(name='gsshaindex-edit-index',
                                  url='gssha-index/{job_id}/edit-index/{map_name}',
                                  controller='gsshaindex.controllers.draw_index:DrawController',
                                  action='edit_index') 
        
        controllers.addController(name='gsshaindex-shapefile-index',
                                  url='gssha-index/{job_id}/shapefile-index/{map_name}/{shapefile_id}',
                                  controller='gsshaindex.controllers.shapefile_index:ShapefileController',
                                  action='shapefile_index') 
        
        controllers.addController(name='gsshaindex-shapefile-upload',
                                  url='gssha-index/{job_id}/shapefile-upload/{map_name}/{shapefile_id}',
                                  controller='gsshaindex.controllers.shapefile_index:ShapefileController',
                                  action='shapefile_upload')
        
        controllers.addController(name='gsshaindex-get-srid-from-wkt',
                                  url='gssha-index/get-srid-from-wkt/{url}',
                                  controller='gsshaindex.controllers.shapefile_index:ShapefileController',
                                  action='get_srid_from_wkt') 
        
        controllers.addController(name='gsshaindex-show-overlay',
                                  url='gssha-index/{job_id}/show-overlay/{shapefile_id}',
                                  controller='gsshaindex.controllers.shapefile_index:ShapefileController',
                                  action='show_overlay')
        
        controllers.addController(name='gsshaindex-combine-index',
                                  url='gssha-index/{job_id}/combine-index/{map_name}',
                                  controller='gsshaindex.controllers.combine_index:CombineController',
                                  action='combine_index')
        
        controllers.addController(name='gsshaindex-submit-edits',
                                  url='gssha-index/{job_id}/submit-edits/{map_name}',
                                  controller='gsshaindex.controllers.draw_index:DrawController',
                                  action='submit_edits')  
        
        controllers.addController(name='gsshaindex-mapping-table',
                                  url='gssha-index/{job_id}/mapping-table/{map_name}/{mapping_table}',
                                  controller='gsshaindex.controllers.index:GSSHAIndexController',
                                  action='mapping_table') 
        
        controllers.addController(name='gsshaindex-replace-values',
                                  url='gssha-index/{job_id}/replace-values/{map_name}/{mapping_table}',
                                  controller='gsshaindex.controllers.index:GSSHAIndexController',
                                  action='replace_values')  
        
        controllers.addController(name='gsshaindex-submit-mapping-table',
                                  url='gssha-index/{job_id}/submit-mapping-table/{map_name}/{mapping_table}',
                                  controller='gsshaindex.controllers.index:GSSHAIndexController',
                                  action='submit_mapping_table')  
        
        controllers.addController(name='gsshaindex-zip-file',
                                  url='gssha-index/{job_id}/zip-file',
                                  controller='gsshaindex.controllers.index:GSSHAIndexController',
                                  action='zip_file')
        
        controllers.addController(name='gsshaindex-status',
                                  url='gssha-index/status',
                                  controller='gsshaindex.controllers.index:GSSHAIndexController',
                                  action='status')
        
        controllers.addController(name='gsshaindex-fly',
                                  url='gssha-index/{job_id}/fly',
                                  controller='gsshaindex.controllers.index:GSSHAIndexController',
                                  action='fly')
        
        controllers.addController(name='gsshaindex-delete',
                                  url='gssha-index/{job_id}/delete',
                                  controller='gsshaindex.controllers.index:GSSHAIndexController',
                                  action='delete')
        
        controllers.addController(name='gsshaindex-results',
                                  url='gssha-index/{job_id}/results',
                                  controller='gsshaindex.controllers.index:GSSHAIndexController',
                                  action='results')
        
    def registerTemplateDirectories(self, templateDirs):
        '''
        Add template directories
        '''

        templateDirs.addTemplateDirectory(directory='gsshaindex/templates')

        
    def registerPublicDirectories(self, publicDirs):
        '''
        Add public directories
        '''
         
        publicDirs.addPublicDirectory(directory='gsshaindex/public')
        
    def registerResources(self, staticDirs):
    	'''
    	Add static directories
    	'''
    	
    	staticDirs.addResource(directory='gsshaindex/public/gsshaindex',
    						   name='ckanapp_gsshaindex')
        
    def registerPersistentStores(self, persistentStores):
        '''
        Add one or more persistent stores
        '''
        persistentStores.addPersistentStore("gsshaidx_db")
        persistentStores.addPersistentStore("gsshapy_db")
        persistentStores.addPersistentStore("shapefile_db")
        persistentStores.addInitializationScript('gsshaindex.lib.init_db')
        
        
         