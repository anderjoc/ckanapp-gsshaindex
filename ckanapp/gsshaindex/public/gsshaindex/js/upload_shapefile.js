
$(document).ready(function(){$('#projections').select2();});

function changeMap(element, file_id, description, file_name, file_url, job_id, map_name) {
	$('.shapefile').each(function(file_id){
		$(this).removeClass('active');
	});
	$('#file_id').val(file_id);
	$('#file_name').val(file_name);
	$('#file_url').val(file_url);
	$('.shapefile').each(function(){
		if ($(this).children('a').get(0).id == file_id){
			$(this).addClass('active');
			$('#description').text(description);
			$('h1').text("Shapefile: "+$(element).text());
		};	
	});
};

window.onload = function(){
	String.prototype.endsWith = function(str) 
		{return (this.match(str+"$")==str)};
	watcher = document.getElementById('selected_files');
	var prj_text_area = document.getElementById('projection-text');
	watcher.addEventListener('change', function(e){
		stuff = document.getElementById('selected_files').files || [];
		console.log(stuff);
		for (var i=0; i<stuff.length; i++){
			file_name = stuff[i].name;
			if (file_name.endsWith('.prj') == true){
				var reader = new FileReader();
				prj_file = stuff.item(i);
				reader.onload=function(e){
					console.log(reader.result);
					prj_text_area.value = reader.result;
				};
				reader.readAsText(prj_file);
			};
		};
	});
};

function submit_files(){
	stuff = document.getElementById('selected_files').files || [];
	var prj_text_area = document.getElementById('projection-text');
	console.log(stuff);
	
	String.prototype.endsWith = function(str) 
		{return (this.match(str+"$")==str)};
		
	prj = false;
	dbf = false;
	shx = false;
	shp = false;
	required_true = true;

	for (var i=0; i<stuff.length; i++){
		file_name = stuff[i].name;
		console.log(file_name);
		if (file_name.endsWith('.shx') == true){
			shx = true;
			shx_file = stuff.item(i);
		};
		if (file_name.endsWith('.shp') == true){
			shp = true;
			shp_file = stuff.item(i);
		};
		if (file_name.endsWith('.dbf') == true){
			dbf = true;
			dbf_file = stuff.item(i);
		};
		if (file_name.endsWith('.prj') == true){
			var reader = new FileReader();
			prj = true;
			prj_file = stuff.item(i);
		};
	};

	if (dbf==false){
		$("#error_message").show();
		required_true = false;
	};
	if (shp==false){
		$("#error_message").show();
		required_true = false;
	};
	if (shx==false){
		$("#error_message").show();
		required_true = false;
	};
	
	if (required_true == true){
		$("#error_message").hide();
		if (prj==false){
			$('#select_files').modal('toggle');
			$('#select_projection').modal('toggle');
		};
		if (prj==true){
			$('#select_files').modal('toggle');
			send_prj_request();
	
		};
	};	
};

// This uses prj2epsg.org to find the code from the projection file
function send_prj_request(){
	var prj_desc = document.getElementById('projection-text').value;
	console.log("http://prj2epsg.org/search.json?mode=wkt&terms=" + prj_desc);
	$.ajax({
		url:"/apps/gssha-index/get-srid-from-wkt/" + encodeURIComponent(prj_desc),
		success: function(data){
			if (data == "error"){
				alert("There was an error with the projection file. Please select a projection.");
				$('#select_projection').modal('toggle');
				
			}
			else {
				console.log(data);
				
				$('#projection-number').attr('value', data);

				$('#name_shapefile').modal('toggle');
			};
		},
		error: function(data){
			alert("There was an error with the projection file. Please select a projection.");
			$('#select_projection').modal('toggle');
		}
	});
};

function submit_projection(){
	prj = $('#projections option:selected').val();
	console.log(prj);
	$('#projection-number').attr('value', prj);
	$('#select_projection').modal('toggle');
	$('#name_shapefile').modal('toggle');
};

