import urllib2, os, requests, json
# from geoserver.catalog import Catalog


geoserver_credentials = {'user':'admin', 'password':'tG%PnIt5pS2K', 'site':'http://ciwmap.chpc.utah.edu:8080/geoserver/'}
geoserver_auth = (geoserver_credentials['user'],geoserver_credentials['password'])

## Create a workspace ##
def create_geoserver_workspace(app):
    workspaces = geoserver_credentials['site'] + 'rest/workspaces'
    r = requests.get(workspaces, auth=geoserver_auth)
    if app not in r.text:
        print "Workspace name isn't there"
        response = requests.post(workspaces, 
                                 auth=geoserver_auth, data="<workspace><name>" + app + "</name></workspace>", 
                                 headers={"Content-type":"text/xml"})
        print response
    else:
        print "Workspace is there!"


## Create a datastore ##
def create_datastore(app, user_id):
    datastores = geoserver_credentials['site'] + 'rest/workspaces/' + app + '/datastores'
    r = requests.get(datastores, auth=geoserver_auth)
    if user_id not in r.text:
        print "Datastore isn't present"
        response = requests.post(datastores, 
                                 auth=geoserver_auth, 
                                 data = """<dataStore>
                                            <name>{0}</name>
                                            <connectionParameters>
                                                 <host>localhost</host>
                                                 <port>5432</port>
                                                 <database>gsshaindex_gsshaidx_db</database>
                                                 <schema>public</schema>
                                                 <user>tethys_db_manager</user>
                                                 <dbtype>shapefile</dbtype>
                                            </connectionParameters>
                                            </dataStore>""".format(user_id), 
                                headers={"Content-type":"application/xml"})
        print response
    else:
        print "The datastore exists"
        
        
## Create a layer ##
def create_layer(app, user_id, file_path, shapefile_name):
    shapefile_url = geoserver_credentials['site'] + 'rest/workspaces/' + app + '/datastores/' + user_id + '/file.shp'
    print "File path: ", file_path
    f_dir, f_name = os.path.split(file_path)
    print "File dir: ", f_dir
    print "File name: ", f_name
    os.chdir(f_dir)
    files = [('upload', file(f_name))]
    response = requests.put(shapefile_url, 
                             auth=geoserver_auth,
                             files = files,
                             headers={"Content-type":"application/zip"})
    print "The next line is the response you're looking for"
    print response
    print response.text


## Get a url for shapefile ##




