import os
import ConfigParser
from collections import namedtuple
from ckanext.tethys_apps.lib.dataset_tools import *
import ckan.plugins as p
from ckan.lib import helpers as h
import pylons.config as config
import zipfile
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from gsshapy.lib import db_tools as dbt
from owslib.wps import WebProcessingService
from owslib.wps import monitorExecution
from ..models import Jobs, jobs_sessionmaker, gsshapy_sessionmaker, gsshapy_engine
from ckanext.tethys_apps.lib.persistent_store import get_persistent_store_engine as gpse

# Get app.ini
controller_dir = os.path.abspath(os.path.dirname(__file__))
config_path = os.path.join(os.path.abspath(os.path.dirname(controller_dir)), 'app.ini')
app_config = ConfigParser.RawConfigParser()
app_config.read(config_path)

public_ckan = app_config.get('development', 'public_ckan')
user_api = app_config.get('development', 'user_api')
develop_state = app_config.get('development', 'develop')

def get_persistent_store_engine(persistent_store_name):
    '''
    Wrapper for the get_persistent_store_engine method that makes it easier to use
    '''
    # Derive app name
    app_name = os.path.split(os.path.dirname(os.path.dirname(__file__)))[1]

    # Get engine
    return gpse(app_name, persistent_store_name)

def find_in_cache(file_id, cache):
    '''
    This function checks to see if the file_id is in the cache.
    If it's not there, it returns false, but if it's there, it returns the kml_link
    file_id = The file that it searches for
    cache = The cache it should search through
    '''
    if file_id in cache:
        kml_link = cache[file_id]
    else:
        kml_link = False
    return kml_link


def clear_folder(workspace_folder):
    '''
    This function deletes a folder if it exists and then recreates it
    '''
    try:
        delete_directory(workspace_folder)
    except:
        pass
    os.makedirs(workspace_folder)


def extract_mask (url, extract_path):
    '''
    This function finds the mask file from the zip file at the url and extracts it to a specified location
    url = location of the zipped GSSHA file
    extract_path = where the mask file should be extracted to
    '''
    
    # Find zip file at the url and find the mask file    
    zip_file = urllib2.urlopen(url)
    zf = zipfile.ZipFile(StringIO.StringIO(zip_file.read()))
    try:
        for file in zf.namelist():
            if file.endswith('.msk'):
                mask_file=file
                 
        # Extract the mask file
        zf.extract(mask_file, extract_path)
        
        return mask_file

    except:
        mask_file = "blank"
        return mask_file
    
    
def append_shapefile_CKAN(context, shp_file, shp_name, description, srid):
        
    my_ckan = config["ckan.site_url"]
    print my_ckan
    print context
    
    if my_ckan == '':
        my_ckan = 'http://localhost:5000'
        
    result = append_file_to_package(my_ckan + '/api/3',
                                 context,
                                 "shapefile",
                                 file_path = shp_file,
                                 name = shp_name,
                                 description = description,
                                 model = "shapefile",
                                 format = "zip",
                                 srid = srid)
    
    return result
    
    
def add_kml_CKAN(user_id, password, context, kml_file, kml_name, fileName=""):
    '''
    This function adds a kml file to CKAN
    user_id = often c.author or c.user
    password = often c.password
    kml_file = where the kml is located
    kml_name = the name of the kml file
    '''
    
    if develop_state == "True":
        userobj = namedtuple('literal', 'apikey')(apikey=user_api,)
        context = namedtuple('literal', 'userobj')(userobj=userobj,)
        public_ckan = app_config.get('development', 'public_ckan')
    else:
        public_ckan = "http:localhost:5000"

    
    
    result = append_file_to_package(public_ckan + '/api',
                                        context,
                                        'kmls',
                                        file_path=kml_file, 
                                        name=kml_name,
                                        format="kml",
                                        file_name = fileName)
    
    return result['result'], result['success']

def add_zip_GSSHA(user_id, password, context, GSSHA_file, GSSHA_name, new_description, date,  fileName=""):
     
    if develop_state == "True":
        userobj = namedtuple('literal', 'apikey')(apikey=user_api,)
        context = namedtuple('literal', 'userobj')(userobj=userobj,)
        public_ckan = app_config.get('development', 'public_ckan')
    else:
        public_ckan = "http:localhost:5000"
     
    result = append_file_to_package(public_ckan + '/api',
                                        context,
                                        'gssha-models',
                                        file_path=GSSHA_file, 
                                        name=GSSHA_name,
                                        model="GSSHA",
                                        format="zip",
                                        file_name = GSSHA_name,
                                        description=new_description + "  Modified on "+ date +" by "+ user_id)
     
    return result['result'], result['success']

def flyGssha(link,resultsFile):
    '''
    This function submits the link to the zipped GSSHA file and gets the result
    '''
    wps = WebProcessingService('http://ci-water.byu.edu:9999/wps/WebProcessingService', verbose=False, skip_caps=True)
            
    processid = 'rungssha'
    inputs = [('url', link)]
    
    output = "outputfile"
    
    execution = wps.execute(processid, inputs, output)
    
    monitorExecution(execution)
       
    result = execution.getOutput(resultsFile)
    
    print "GSSHA has taken off!"
    
def extract_otl (url, extract_path):
    '''
    This function finds the otl file from the zip file at the url and extracts it to a specified location
    url = location of the zipped GSSHA file
    extract_path = where the mask file should be extracted to
    '''
    # Find zip file at the url and find the mask file    
    zip_file = urllib2.urlopen(url)
    zf = zipfile.ZipFile(StringIO.StringIO(zip_file.read()))
    for file in zf.namelist():
#         if file.startswith("Results"):
            if file.endswith('.otl'):
                otl_file=file
             
    # Extract the mask file
    zf.extract(otl_file, extract_path)
    
    return otl_file


    


    