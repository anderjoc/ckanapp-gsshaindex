from ckanapp.gsshaindex.models import Base, gsshapy_engine, shapefile_engine, Engine
from gsshapy.orm import metadata as gsshapy_metadata

# Set up the jobs database
Base.metadata.create_all(Engine)

# Set up the gsshapy database
gsshapy_metadata.create_all(gsshapy_engine)

# Set up the shapefile database
gsshapy_metadata.create_all(shapefile_engine)