from setuptools.command.develop import develop
from setuptools.command.install import install
from ckanext.tethys_apps.lib import get_ckanapp_directory
from ckanext.tethys_apps.lib.persistent_store import provision_persistent_stores
from ckanext.tethys_apps.lib import get_ckanapp_directory
from setuptools import setup, find_packages
import sys, os, shutil

### App Packages ###
# Don't change these
app_package = 'gsshaindex'
release_package = 'ckanapp-' + app_package
app_class = 'gsshaindex.app:GSSHAIndexApp'

### App Metadata ###
# Change whatever you'd like here
version = '0.1'
author = 'Jocelynn M Anderson'
author_email = 'jocelynn.anderson@byu.edu'
short_description = 'GSSHA index map editor'
long_description = ''
url = 'http://127.0.0.1:5000/apps/gsshaindex'
license = 'BSD 2-Clause'
keywords = ''

### Python Dependencies ###
# List external Python dependencies here by name
dependencies = [
                "gsshapy",
                "sqlalchemy==0.7.8",
                "owslib",
                "gsconfig"
    # -*- Add names of external Python dependencies here -*-
]

class CustomInstallCommand(install):
    """
    When install command is used on setup.py, will copy app package to ckanapp directory.
    """
    def run(self):
        # Get paths
        ckanapp_dir = get_ckanapp_directory()
        app_package_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'ckanapp', app_package)
        destination_dir = os.path.join(ckanapp_dir, app_package)

        # Notify user
        print 'Copying App Package: {0} to {1}'.format(app_package_dir, destination_dir)

        # Copy files
        try:
            shutil.copytree(app_package_dir, destination_dir)
        except:
            try:
                shutil.rmtree(destination_dir)
            except:
                os.remove(destination_dir)

            shutil.copytree(app_package_dir, destination_dir)

        # Run the original install command
        install.run(self)

class CustomDevelopCommand(develop):
    """
    When develop command is used on setup.py, will create symbolic link from app package to ckanapp directory.
    """
    def run(self):
        # Get paths
        ckanapp_dir = get_ckanapp_directory()
        app_package_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'ckanapp', app_package)
        destination_dir = os.path.join(ckanapp_dir, app_package)

        # Notify user
        print 'Creating Symbolic Link to App Package: {0} to {1}'.format(app_package_dir, destination_dir)

        # Create symbolic link
        try:
            os.symlink(app_package_dir, destination_dir)
        except:
            try:
                shutil.rmtree(destination_dir)
            except:
                os.remove(destination_dir)

            os.symlink(app_package_dir, destination_dir)

        # Run the original develop command
        develop.run(self)


setup(
    name=release_package,
    version=version,
    description=short_description,
    long_description=long_description,
    classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords=keywords,
    author=author,
    author_email=author_email,
    url=url,
    license=license,
    packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
    namespace_packages=['ckanapp', 'ckanapp.' + app_package],
    include_package_data=True,
    zip_safe=False,
    install_requires=dependencies,
    entry_points=\
    """
    """,
    cmdclass={
        'install': CustomInstallCommand,
        'develop': CustomDevelopCommand
    }
)

# Provision tethys databases for app
provision_persistent_stores(app_class)
